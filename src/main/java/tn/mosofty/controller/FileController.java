package tn.mosofty.controller;

import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.mosofty.services.FileStorge;

@RestController
@CrossOrigin("*")
@RequestMapping("/file")
public class FileController {

   private final FileStorge fileStorge;


    public FileController(FileStorge fileStorge) {
        this.fileStorge = fileStorge;
    }

    @GetMapping("/{filename}")
    public ResponseEntity<?> getFile(@PathVariable(name = "filename") String filename) {
        Resource file = fileStorge.load(filename);
        if (file != null) {
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        } else {
            return new ResponseEntity<>("Could not read the file!", HttpStatus.NOT_FOUND);
        }

    }
}
