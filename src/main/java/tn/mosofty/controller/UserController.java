package tn.mosofty.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tn.mosofty.model.User;
import tn.mosofty.payload.LoginRequest;
import tn.mosofty.services.FileStorge;
import tn.mosofty.services.UserService;



@RestController
@CrossOrigin("*")
@RequestMapping("/user")
public class UserController {
    private final UserService userService;
    private final FileStorge fileStorge;


    public UserController(UserService userService, FileStorge fileStorge) {
        this.userService = userService;
        this.fileStorge = fileStorge;
    }
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest){
        if(userService.login(loginRequest.getUsername(),loginRequest.getPassword())!=null){
            return  ResponseEntity.status(HttpStatus.OK).
                    body(userService.login(loginRequest.getUsername(), loginRequest.getPassword()));
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
        }

    }

    @PostMapping("/registre")
    public ResponseEntity<User> registre(@RequestParam(name = "user") String user,
                                         @RequestParam("photo") MultipartFile photo) {
        try {
            User usertosave = new ObjectMapper().readValue(user, User.class);
            String namefile = fileStorge.save(photo);
            usertosave.setPhotourl(namefile);
            return ResponseEntity.status(HttpStatus.CREATED).body(userService.ajouterUtisateur(usertosave));

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
    @PutMapping("/modifier/{id}")
    public ResponseEntity<?> modifier(@PathVariable(name = "id")Long id,@RequestBody User useramodifier){
        System.out.println(id);
        if(userService.userexistebyid(id)){
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.modifierUtilisateur(useramodifier,id));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
        }
    }
    @PutMapping("/modifieravecfile/{id}")
    public ResponseEntity<?> modifier(@PathVariable(name = "id")Long id,@RequestParam(name = "user") String user,
                                      @RequestParam("photo") MultipartFile photo)  {
        try {
            User usertosave = new ObjectMapper().readValue(user, User.class);
            String namefile = fileStorge.save(photo);
            usertosave.setPhotourl(namefile);
            if(userService.userexistebyid(id)){
                return ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.modifierUtilisateur(usertosave,id));
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
    @GetMapping("/getuserbyid/{id}")
    public ResponseEntity<?> getuserbyid(@PathVariable long id ){
        return ResponseEntity.status(HttpStatus.OK).body(userService.getuserbyid(id));

    }
}
