package tn.mosofty.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.mosofty.model.Note;
import tn.mosofty.repository.NoteRepository;
import tn.mosofty.services.NoteService;

@RestController
@CrossOrigin("*")
@RequestMapping("/note")
public class NoteController {
    private final NoteService noteService;
    private final NoteRepository noteRepository;

    public NoteController(NoteService noteService, NoteRepository noteRepository) {
        this.noteService = noteService;
        this.noteRepository = noteRepository;
    }
    @GetMapping("/getall")
    public ResponseEntity<?> getall(){
        return ResponseEntity.status(HttpStatus.OK).body(noteService.getallNote());
    }
    @GetMapping("/getNoteProduit")
    public ResponseEntity<?> getallproduit(){
        return ResponseEntity.status(HttpStatus.OK).body(noteService.listproduitsAvecnote());
    }
    @GetMapping("/getNoteUser")
    public ResponseEntity<?> getalluser(){
        return ResponseEntity.status(HttpStatus.OK).body(noteService.listUserAvecNote());
    }

    @PostMapping("/saveNote")
    public ResponseEntity<?> save(@RequestBody Note n){
        return ResponseEntity.status(HttpStatus.CREATED).body(noteService.addNoteUserToProduit(n));
    }

}
