package tn.mosofty.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tn.mosofty.model.Produit;
import tn.mosofty.model.User;
import tn.mosofty.services.FileStorge;
import tn.mosofty.services.ProduitService;

@RestController
@CrossOrigin("*")
@RequestMapping("/produit")
public class ProduitController {
    private final ProduitService produitService;
    private final FileStorge fileStorge;

    public ProduitController(ProduitService produitService, FileStorge fileStorge) {
        this.produitService = produitService;
        this.fileStorge = fileStorge;
    }
    @GetMapping("/GetAll")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(produitService.getAllProduit());

    }
    @PostMapping("/save")
    public ResponseEntity<Produit> registre(@RequestParam(name = "produit") String produit,
                                            @RequestParam("photo") MultipartFile photo) {
        try {
            Produit produittosave = new ObjectMapper().readValue(produit, Produit.class);
            String namefile = fileStorge.save(photo);
            produittosave.setPhotourl(namefile);
            return ResponseEntity.status(HttpStatus.CREATED).body(produitService.saveProduit(produittosave));

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }  @PutMapping("/modifier/{id}")
    public ResponseEntity<?> modifier(@PathVariable(name = "id")Long id,@RequestParam(name = "produit") String produit,
                                      @RequestParam("photo") MultipartFile photo){
        System.out.println(id);
        try {


        if(produitService.produitexistebyid(id)){
            Produit produittosave = new ObjectMapper().readValue(produit, Produit.class);
            String namefile = fileStorge.save(photo);
            produittosave.setPhotourl(namefile);
            return ResponseEntity.status(HttpStatus.ACCEPTED)
                    .body(produitService.updateProduit(id,produittosave));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
        }
        }catch (Exception e){
            return null;
        }
    }



}
