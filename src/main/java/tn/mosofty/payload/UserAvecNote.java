package tn.mosofty.payload;

import lombok.Data;
import tn.mosofty.model.Note;
import tn.mosofty.model.User;

import java.util.List;

@Data
public class UserAvecNote {
    User user;
    List<Note> notes;
}
