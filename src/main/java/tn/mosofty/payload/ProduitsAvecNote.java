package tn.mosofty.payload;

import lombok.Data;
import tn.mosofty.model.Note;
import tn.mosofty.model.Produit;

import java.util.List;
@Data
public class ProduitsAvecNote {
    Produit p;
    List<Note> note;
}
