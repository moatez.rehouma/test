package tn.mosofty.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tn.mosofty.model.Note;
import tn.mosofty.model.Produit;
import tn.mosofty.model.User;

import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<Note,Long> {
   List<Note> findNotesByProduit(Produit p);
   List<Note> findNotesByUser(User u);


}
