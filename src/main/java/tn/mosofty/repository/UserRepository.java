package tn.mosofty.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.mosofty.model.User;
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findUserByEmailAndPassword(String email, String username);
}
