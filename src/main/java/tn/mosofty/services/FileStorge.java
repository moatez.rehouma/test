package tn.mosofty.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorge {
     void init();

     String save(MultipartFile file);

     Resource load(String filename);



     boolean deleteFileByName(String name);
}
