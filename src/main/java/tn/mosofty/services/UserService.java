package tn.mosofty.services;

import tn.mosofty.model.User;

import java.util.List;

public interface UserService {
    User ajouterUtisateur(User user);
    User modifierUtilisateur(User user , Long id);

    User login(String email , String password);
    boolean userexistebyid(Long id );
    User getuserbyid(Long id);
    List<User> getalluser();

}
