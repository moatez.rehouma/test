package tn.mosofty.services;

import tn.mosofty.model.Note;
import tn.mosofty.payload.ProduitsAvecNote;
import tn.mosofty.payload.UserAvecNote;

import java.util.List;

public interface NoteService {
    Note addNoteUserToProduit(Note n);
    List<Note> getallNote();
    List<ProduitsAvecNote>  listproduitsAvecnote();

    List<UserAvecNote> listUserAvecNote();

}
