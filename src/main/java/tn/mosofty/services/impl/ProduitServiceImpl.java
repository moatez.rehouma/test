package tn.mosofty.services.impl;

import org.springframework.stereotype.Service;
import tn.mosofty.model.Produit;
import tn.mosofty.repository.ProduitRepository;
import tn.mosofty.services.ProduitService;

import java.util.List;

@Service
public class ProduitServiceImpl implements ProduitService {
    private final ProduitRepository produitRepository;

    public ProduitServiceImpl(ProduitRepository produitRepository) {
        this.produitRepository = produitRepository;
    }


    @Override
    public List<Produit> getAllProduit() {
        return produitRepository.findAll();
    }

    @Override
    public Produit saveProduit(Produit p) {
        return produitRepository.save(p);
    }

    @Override
    public Produit updateProduit(Long id, Produit p) {
        if(produitRepository.existsById(id)){
            Produit produit = produitRepository.findById(id).orElseThrow();
            produit.setNomproduit(p.getNomproduit());
            produit.setPrixdebase(p.getPrixdebase());
            produit.setPrixproduit(p.getPrixproduit());
            produit.setDescription(p.getDescription());
            produit.setAlt_seo(p.getAlt_seo());
            produit.setPhotourl(p.getPhotourl());
            return produitRepository.save(produit);
        }
        return null;
    }

    @Override
    public boolean produitexistebyid(long id) {
        return produitRepository.existsById(id);
    }

    @Override
    public Produit getProduitByid(Long id) {
        return produitRepository.findById(id).orElse(null);
    }

}
