package tn.mosofty.services.impl;

import org.aspectj.weaver.ast.Not;
import org.springframework.stereotype.Service;
import tn.mosofty.model.Note;
import tn.mosofty.model.Produit;
import tn.mosofty.model.User;
import tn.mosofty.payload.ProduitsAvecNote;
import tn.mosofty.payload.UserAvecNote;
import tn.mosofty.repository.NoteRepository;
import tn.mosofty.services.NoteService;
import tn.mosofty.services.ProduitService;
import tn.mosofty.services.UserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {
    private final NoteRepository noteRepository;
    private final UserService userService;
    private final ProduitService produitService;

    public NoteServiceImpl(NoteRepository noteRepository, UserService userService, ProduitService produitService) {
        this.noteRepository = noteRepository;
        this.userService = userService;
        this.produitService = produitService;
    }

    @Override
    public Note addNoteUserToProduit(Note n) {
        Note note = new Note();
        note.setUser(userService.getuserbyid(n.getUser().getId()));
        note.setProduit(produitService.getProduitByid(n.getProduit().getId()));
        note.setNote(n.getNote());
        return noteRepository.save(note);
    }

    @Override
    public List<Note> getallNote() {
        return noteRepository.findAll();
    }

    @Override
    public List<ProduitsAvecNote> listproduitsAvecnote() {
        List<Produit> produits = produitService.getAllProduit();

        List<ProduitsAvecNote> produitsAvecNotes = new ArrayList<>();
        for(Produit p :produits){
            ProduitsAvecNote produitsAvecNote = new ProduitsAvecNote();
            produitsAvecNote.setP(p);
            produitsAvecNote.setNote(noteRepository.findNotesByProduit(p));
            produitsAvecNotes.add(produitsAvecNote);
        }
        return produitsAvecNotes;
    }

    @Override
    public List<UserAvecNote> listUserAvecNote() {
        List<User> users = userService.getalluser();
        List<UserAvecNote> userAvecNotes = new ArrayList<>();
        for(User u :users){
            UserAvecNote userAvecNote = new UserAvecNote();
            userAvecNote.setUser(u);
            userAvecNote.setNotes(noteRepository.findNotesByUser(u));


            userAvecNotes.add(userAvecNote);
        }
        return userAvecNotes;
    }
}
