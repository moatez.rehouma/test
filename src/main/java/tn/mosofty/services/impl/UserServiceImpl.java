package tn.mosofty.services.impl;


import org.springframework.stereotype.Service;
import tn.mosofty.model.User;
import tn.mosofty.repository.UserRepository;
import tn.mosofty.services.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User ajouterUtisateur(User user) {
        return userRepository.save(user);
    }

    @Override
    public User modifierUtilisateur(User user , Long id ) {
        if(userRepository.existsById(id)){
            User anicenuser = userRepository.findById(id).orElseThrow();
            anicenuser.setFirstname(user.getFirstname());
            anicenuser.setLastname(user.getLastname());
            anicenuser.setEmail(user.getEmail());
            anicenuser.setAge(user.getAge());
            anicenuser.setCivilite(user.getCivilite());
            anicenuser.setUsername(user.getUsername());
            anicenuser.setPhotourl(user.getPhotourl());
            return userRepository.save(anicenuser);
        }
        return null;
    }

    @Override
    public User login(String email, String password) {


        User user =userRepository.findUserByEmailAndPassword(email,password);
        if(user!=null){
        return user;}
        else{
            return null;
        }
    }

    @Override
    public boolean userexistebyid(Long id) {
        return userRepository.existsById(id);
    }

    @Override
    public User getuserbyid(Long id) {
        return userRepository.findById(id).orElseThrow();
    }

    @Override
    public List<User> getalluser() {
        return userRepository.findAll();
    }
}
