package tn.mosofty.services;

import tn.mosofty.model.Produit;

import java.util.List;

public interface ProduitService {
    List<Produit> getAllProduit();
    Produit saveProduit(Produit p);
    Produit updateProduit(Long id ,Produit p);
    boolean produitexistebyid(long id);
    Produit getProduitByid(Long id);
}
